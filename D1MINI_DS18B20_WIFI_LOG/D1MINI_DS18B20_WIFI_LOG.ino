#include <Arduino.h>

#include <OneWire.h>
#include <DallasTemperature.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>


#define LOOP_DELAY_TIME 58 // 59 seconds

#define USE_SERIAL Serial
#define ONE_WIRE_BUS D3 // DS18B20
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

ESP8266WiFiMulti WiFiMulti;

#include <WiFiManager.h> // https://github.com/tzapu/WiFiManager

String url;
int counter=0;


String ipToString(IPAddress ip){
  String s="";
  for (int i=0; i<4; i++)
    s += i  ? "." + String(ip[i]) : String(ip[i]);
  return s;
}

void setup() {
    // WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP
    // it is a good practice to make sure your code sets wifi mode how you want it.

    // put your setup code here, to run once:
    Serial.begin(115200);
    pinMode(LED_BUILTIN, OUTPUT);

    //WiFiManager, Local intialization. Once its business is done, there is no need to keep it around
    WiFiManager wm;

    // reset settings - wipe stored credentials for testing
    // these are stored by the esp library
    // wm.resetSettings();

    // Automatically connect using saved credentials,
    // if connection fails, it starts an access point with the specified name ( "AutoConnectAP"),
    // if empty will auto generate SSID, if password is blank it will be anonymous AP (wm.autoConnect())
    // then goes into a blocking loop awaiting configuration and will return success result

    bool res;
    res = wm.autoConnect(); // auto generated AP name from chipid
    // res = wm.autoConnect("AutoConnectAP"); // anonymous ap
    // res = wm.autoConnect("AutoConnectAP","password"); // password protected ap

    if(!res) {
      Serial.println("Failed to connect");
      // ESP.restart();
    }
    else {
      //if you get here you have connected to the WiFi
      Serial.println("connected...yeey :)");
    }

}

    
void loop() {

  pinMode(ONE_WIRE_BUS, INPUT);
  sensors.begin();
  sensors.requestTemperatures();
  delay(200);
  float  nowTemp = sensors.getTempCByIndex(0);
  
  if (counter<0 || counter>1440) {
    counter = 0;
  } else {
    counter++;
  }


  if ((WiFiMulti.run() == WL_CONNECTED)) {

    std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);

    client->setFingerprint("ED:05:86:7E:6C:B3:64:BF:2A:CC:D6:24:D4:88:84:D0:A7:A3:4C:F2");
    // Or, if you happy to ignore the SSL certificate, then use the following line instead:
    client->setInsecure();

    HTTPClient https;

    Serial.print("[HTTPS] begin...\n");

    IPAddress ip = WiFi.localIP();
    String rssi = String(WiFi.RSSI()); 
    url = "/log.gif";
    url += "?cnt="+(String)counter;
    url += "&type=temp";
    url += "&id="+(String)WiFi.hostname();
    url += "&ip="+ipToString(ip);
    url += "&ssid="+WiFi.SSID();
    url += "&val="+(String)nowTemp;
    url += "&";

    USE_SERIAL.print("GET "+url+"\n");


    if (https.begin(*client, "ts.monster.tw", 443, url, true)) {  // HTTPS

      Serial.print("[HTTPS] GET...\n");
      // start connection and send HTTP header
      int httpCode = https.GET();

      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTPS] GET... code: %d\n", httpCode);

        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = https.getString();
          // String payload = https.getString(1024);  // optionally pre-reserve string to avoid reallocations in chunk mode
          // Serial.println(payload);
        }
      } else {
        Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
      }

      https.end();
    } else {
      Serial.printf("[HTTPS] Unable to connect\n");
    }
  }
  digitalWrite(LED_BUILTIN, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, HIGH);    // turn the LED off by making the voltage LOW
  delay(LOOP_DELAY_TIME*1000);
}
